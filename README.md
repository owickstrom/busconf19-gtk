# BusConf 2019: Declarative GTK in Haskell

Let's build something with _gi-gtk-declarative_ at BusConf!

## The Packages

* _gi-gtk-declarative_: core framework for declarative GTK widgets in Haskell
* _gi-gtk-declarative-app-simple_: Elm-style architecture for gi-gtk-declarative

## Links

* [Documentation](https://owickstrom.github.io/gi-gtk-declarative/widgets/the-widget-type/)
* [gi-gtk-declarative](https://hackage.haskell.org/package/gi-gtk-declarative)
* [gi-gtk-declarative-app-simple](https://hackage.haskell.org/package/gi-gtk-declarative-app-simple)
* [GTK Widgets](http://hackage.haskell.org/package/gi-gtk-3.0.31/docs/GI-Gtk-Objects.html) (not all are supported!)

